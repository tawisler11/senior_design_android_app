package com.example.seniordesign;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ToggleButton;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("NewApi")
public class MainActivity extends ActionBarActivity {

	final Context context = this;
	
	HttpClient httpClient = new DefaultHttpClient();
    HttpGet httpGet = new HttpGet("https://trine-smart-power.herokuapp.com");
    HttpGet USER_PAGE = new HttpGet("https://trine-smart-power.herokuapp.com/load_states/new");
    HttpPost httpPost = new HttpPost("https://trine-smart-power.herokuapp.com/login");
    HttpPost POST_LOAD = new HttpPost("https://trine-smart-power.herokuapp.com/load_states");
    HttpGet USER_SHOW = new HttpGet("https://trine-smart-power.herokuapp.com/profile");
    
    GraphView graph;
    LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(new DataPoint[] {
		      new DataPoint(0,50)
		    , new DataPoint(1,60)
		    , new DataPoint(2,70)
		    , new DataPoint(3,80)
		    , new DataPoint(4,90)
		    , new DataPoint(5,70)
		    , new DataPoint(6,60)
		    , new DataPoint(7,20)
		    , new DataPoint(8,10)
			, new DataPoint(9,60)
		});
    
    String email = "";
    String password = "";
	
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if (android.os.Build.VERSION.SDK_INT > 9) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()

			.permitAll().build();

			StrictMode.setThreadPolicy(policy);

			}
		
		graph = (GraphView) findViewById(R.id.graph);
		
		login();
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void initialize() {
		loginUser();
		updateGraph();
		initGraph();
	}
	
	public void login() {
		 
		// get prompts.xml view
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.loginprompt, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		final EditText userEmail = (EditText) promptsView
				.findViewById(R.id.emailUserInput);
		
		final EditText userPassword = (EditText) promptsView
				.findViewById(R.id.passwordUserInput);
		
		// set dialog message
		alertDialogBuilder
			.setCancelable(false)
			.setPositiveButton("OK",
			  new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	
			    email = userEmail.getText().toString();
				password = userPassword.getText().toString();
				
				Log.d("MyTag",email);
				Log.d("MyTag",password);
				
				initialize();
				
			    }
			  })
			.setNegativeButton("Cancel",
			  new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			    }
			  });

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}
	
	/*********************************************
	 *	initGraph() initializes the graph used to
	 *	display the Battery Charge Data
	 *
	 *	This function uses the graph object
	 *	that was created globally for this activity.
	 *	
	 *	This function is only called once in 
	 *	onCreate().
	 *********************************************/
	private void initGraph(){
		
		StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
		staticLabelsFormatter.setHorizontalLabels(new String[] {"0","1","2","3","4","5","6","7","8","9"});
		
		graph.getViewport().setXAxisBoundsManual(true);
		graph.getViewport().setYAxisBoundsManual(true);
		
		graph.getViewport().setMinX(0);
		graph.getViewport().setMaxX(9);
		graph.getViewport().setMinY(0);
		graph.getViewport().setMaxY(100);
		
		graph.setTitle("Charge vs. Time");
		graph.getGridLabelRenderer().setHorizontalAxisTitle("Time");
		graph.getGridLabelRenderer().setVerticalAxisTitle("Charge");
		graph.getViewport().setScrollable(true);
		graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
		graph.addSeries(series);
	}
	
	/*********************************************
	 *	loginUser() logs the user in, establishing
	 *	a session with the web site.
	 *
	 *	This function uses the httpClient object
	 *	that was created globally for this activity.
	 *	
	 *	The function follows this process:
	 *		-GET url/#
	 *		-Search for CSRF token, and store it
	 *		-Create NameValuePair for parameters
	 *		-Encode url/paramters
	 *		-POST url/login/
	 *********************************************/
	private void loginUser( ) {
		HttpResponse response;
        Pattern pattern = Pattern.compile("meta name=\"csrf-token\" content=\"(.*?)\" /");
        Matcher matcher = pattern.matcher("");
        String csrf = "";
        //**************************************
      	//			GET url/#
      	//**************************************
        try {
        	response = httpClient.execute(httpGet);
        	String responseString = EntityUtils.toString(response.getEntity());
        	
        	matcher = pattern.matcher(responseString);
        	
        	//**************************************
    		//		Search/Store CSRF token
    		//**************************************
        	if(matcher.find())
            {
        		csrf = matcher.group(1);
            	Log.d("MyTag",matcher.group(1));
            }
        	
        } catch (ClientProtocolException e) {
            // Log exception
            e.printStackTrace();
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
        }
        
        //**************************************
      	//	Create NameValuePair/Parameters
      	//**************************************
        List<NameValuePair> nameValuePair1 = new ArrayList<NameValuePair>(2);
        nameValuePair1.add(new BasicNameValuePair("utf8", "true"));
    	nameValuePair1.add(new BasicNameValuePair("authenticity_token", csrf)); //Note that this is the CSRF token found from GET Method
        nameValuePair1.add(new BasicNameValuePair("session[email]", email));
        nameValuePair1.add(new BasicNameValuePair("session[password]", password));
        nameValuePair1.add(new BasicNameValuePair("commit", "Log in"));
        
        //**************************************
      	//			Encode Post Data
      	//**************************************
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair1));
        } catch (UnsupportedEncodingException e) {
            // log exception
            e.printStackTrace();
        }
        
        
        //**************************************
      	//			POST url/login
      	//**************************************
        try {
            HttpResponse response2 = httpClient.execute(httpPost);
            
            if(response2.getStatusLine().getStatusCode() == 200){
                Log.v("HTTPResponse", "200");
            } else {
                Log.v("HTTPResponse","" + response2.getStatusLine().getStatusCode());
            }
            
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	
	/*********************************************
	 *	updateGraph() searches the user profile
	 *	page of the web site for the Battery
	 *	Charge data, and then updates the 
	 *	graph with that data.
	 *
	 *	This function uses the httpClient object
	 *	that was created globally for this activity.
	 *	
	 *	The function follows this process:
	 *		-GET url/users/(.userId)
	 *		-Search for Charge: ## and store
	 *		-Update Graph
	 *********************************************/
	private void updateGraph(){
		Pattern pattern = Pattern.compile("Charge: (.*?)</b>");
		Matcher matcher = pattern.matcher("");
		HttpResponse response;
		int[] charges = new int[10];
		
		//**************************************
		//			UPDATE GRAPH
		//**************************************
		try {
        	response = httpClient.execute(USER_SHOW);
        	String responseString = EntityUtils.toString(response.getEntity());
        	
        	matcher = pattern.matcher(responseString);
        	
        	//**************************************
    		//		Search/Store Charge ##
    		//**************************************
        	int i = 0;
        	while(matcher.find()){
        		charges[i] = Integer.parseInt(matcher.group(1));
        		Log.d("MyTag",Integer.toString(charges[i]));
        		i++;
        	}
        } catch (ClientProtocolException e) {
            // Log exception
            e.printStackTrace();
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
        }
		
		
		//**************************************
		//			UPDATE GRAPH
		//**************************************
		series.resetData(new DataPoint[] {
				  new DataPoint(0,charges[9])
			    , new DataPoint(1,charges[8])
			    , new DataPoint(2,charges[7])
			    , new DataPoint(3,charges[6])
			    , new DataPoint(4,charges[5])
			    , new DataPoint(5,charges[4])
			    , new DataPoint(6,charges[3])
			    , new DataPoint(7,charges[2])
			    , new DataPoint(8,charges[1])
				, new DataPoint(9,charges[0])
		});
	}
	
	/*********************************************
	 *	makePostRequest() takes two arguments of
	 *	BasicNameValuePair. These are used in the
	 *	parameters of the post request, which in
	 *	the end control the Load_State{load,action}
	 *	on the web site end (or the load physically).
	 *
	 *	This function uses the httpClient object
	 *	that was created globally for this activity.
	 *	
	 *	The function follows this process:
	 *		-GET url/load_states/new
	 *		-search response for CSRF token
	 *			and store it.
	 *		-Create a NameValuePair that hosts
	 *			the parameters of the POST
	 *			request to be made.
	 *		-Encode Url with NameValuePair
	 *		-POST url/load_states/
	 *
	 *********************************************/
	private void makePostRequest( BasicNameValuePair onOrOff, BasicNameValuePair loadNumber ) {
		
        //Get for CSRF TOKEN
        HttpResponse response;
        Pattern pattern = Pattern.compile("meta name=\"csrf-token\" content=\"(.*?)\" /");
        Matcher matcher = pattern.matcher("");
        
        
        //**************************************
      	//		GET url/load_states/new
      	//**************************************
        String csrf = "";
        String responseString = "";
        try {
        	response = httpClient.execute(USER_PAGE);
        	
        	responseString = EntityUtils.toString(response.getEntity());
        	matcher = pattern.matcher(responseString);
        	
        	//**************************************
    		//		Search for CSRF token
    		//**************************************
        	if(matcher.find())
            {
        		csrf = matcher.group(1);
            	Log.d("MyTag",csrf);
            }
        	
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        
        //**************************************
      	//		Create NameValuePair - Params
      	//**************************************
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(5);
        nameValuePair.add(new BasicNameValuePair("utf8", "true"));
    	nameValuePair.add(new BasicNameValuePair("authenticity_token", csrf)); 	//Note that this is the CSRF token found from GET Method
        nameValuePair.add(loadNumber); 											//Note that this is loadNumber passed to the function
        nameValuePair.add(onOrOff); 											//Note that this is onOrOff passed to the function
        nameValuePair.add(new BasicNameValuePair("commit", "Create a Load"));
        
        //**************************************
      	//			Encode Data
      	//**************************************
        try {
        	POST_LOAD.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        
        //**************************************
      	//			POST url/load_states/
      	//**************************************
        try {
            HttpResponse response3 = httpClient.execute(POST_LOAD);
            
            if(response3.getStatusLine().getStatusCode() == 200){
                Log.v("HTTPResponse", "200");
            } else {
                Log.v("HTTPResponse","" + response3.getStatusLine().getStatusCode());
                Log.e("Http Post Response:", EntityUtils.toString(response3.getEntity()));
                login();
            }
            Log.d("MyTag", "Posted!");
            
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
	
	/*****************************************
	 *	onRefreshTouched() is called
	 *	when the button labeled REFREESH GRAPH
	 *	is touched.
	 *
	 *	It then calls updateGraph()
	 *
	 *****************************************/
	public void onRefreshTouched(View view) {
		updateGraph();
	}
	
	
	/*********************************************
	 *	onToggleClicked1() is called when the button
	 *	labeled "Load 1" is pressed.
	 *
	 *	It makes a decision on whether or not
	 *	the load should be on (based on the toggle
	 *	button's state) and passes that as well as
	 *	the Load Number to makePostRequest()
	 *********************************************/
	//Load 1 toggleButton
	public void onToggleClicked1(View view) {
		Log.d("MyTag","Button1");
		
	    // Is the toggle on?
	    boolean on = ((ToggleButton) view).isChecked();
	    BasicNameValuePair onOrOff,loadNumber;
	    
	    //Create Name Value Pairs that are then sent to the POST function//
	    if (on) {
	    	onOrOff = new BasicNameValuePair("load_state[action]", "true");
	    	loadNumber = new BasicNameValuePair("load_state[load]", "1");
	    } else {
	    	onOrOff = new BasicNameValuePair("load_state[action]", "false");
	    	loadNumber = new BasicNameValuePair("load_state[load]", "1");
	    }
	    makePostRequest(onOrOff,loadNumber);
	}
	
	/*********************************************
	 *	onToggleClicked2() is called when the button
	 *	labeled "Load 2" is pressed.
	 *
	 *	It makes a decision on whether or not
	 *	the load should be on (based on the toggle
	 *	button's state) and passes that as well as
	 *	the Load Number to makePostRequest()
	 *********************************************/
	//Load 2 toggleButton
	public void onToggleClicked2(View view) {
		Log.d("MyTag","Button2");
		
	    // Is the toggle on?
	    boolean on = ((ToggleButton) view).isChecked();
	    BasicNameValuePair onOrOff,loadNumber;
	    
	  //Create Name Value Pairs that are then sent to the POST function//
	    if (on) {
	    	onOrOff = new BasicNameValuePair("load_state[action]", "true");
	    	loadNumber = new BasicNameValuePair("load_state[load]", "2");
	    } else {
	    	onOrOff = new BasicNameValuePair("load_state[action]", "false");
	    	loadNumber = new BasicNameValuePair("load_state[load]", "2");
	    }
	    makePostRequest(onOrOff,loadNumber);
	}
	
	/*********************************************
	 *	onToggleClicked3() is called when the button
	 *	labeled "Load 3" is pressed.
	 *
	 *	It makes a decision on whether or not
	 *	the load should be on (based on the toggle
	 *	button's state) and passes that as well as
	 *	the Load Number to makePostRequest()
	 *********************************************/
	//Load 3 toggleButton
	public void onToggleClicked3(View view) {
		Log.d("MyTag","Button3");
		
	    // Is the toggle on?
	    boolean on = ((ToggleButton) view).isChecked();
	    BasicNameValuePair onOrOff,loadNumber;
	    
	  //Create Name Value Pairs that are then sent to the POST function//
	    if (on) {
	    	onOrOff = new BasicNameValuePair("load_state[action]", "true");
	    	loadNumber = new BasicNameValuePair("load_state[load]", "3");
	    } else {
	    	onOrOff = new BasicNameValuePair("load_state[action]", "false");
	    	loadNumber = new BasicNameValuePair("load_state[load]", "3");
	    }
	    makePostRequest(onOrOff,loadNumber);
	}
	
	/*********************************************
	 *	onCheckClicked() is called when the check
	 *	box labeled "Smart Power" is pressed.
	 *
	 *	It makes a decision on whether or not
	 *	the load should be on (based on the toggle
	 *	button's state) and passes that as well as
	 *	the Load Number to makePostRequest()
	 *********************************************/
	//Load 3 toggleButton
	public void onCheckClicked(View view) {
		Log.d("MyTag","Button3");
		
	    // Is the toggle on?
	    boolean on = ((CheckBox) view).isChecked();
	    BasicNameValuePair onOrOff,loadNumber;
	    
	  //Create Name Value Pairs that are then sent to the POST function//
	    if (on) {
	    	onOrOff = new BasicNameValuePair("load_state[action]", "true");
	    	loadNumber = new BasicNameValuePair("load_state[load]", "4");
	    } else {
	    	onOrOff = new BasicNameValuePair("load_state[action]", "false");
	    	loadNumber = new BasicNameValuePair("load_state[load]", "4");
	    }
	    makePostRequest(onOrOff,loadNumber);
	}
	
}
